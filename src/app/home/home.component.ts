import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onLoadServers($event) {
    // complex calculations...then

    // http://localhost:4200/servers/1/edit?allowEdit=1#loading
    //
    this.router.navigate(['/servers', 1, 'edit'], { queryParams: {allowEdit: '1'}, fragment: 'loading' });
  }

}
